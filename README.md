# App React Native com Expo
## Para iniciar o app, usar:

```
cd gerador-de-senha/
npm i 
npx expo start
```
### Prints

<center>
    <img src="./.prints/home.jpeg" width="150" title="Tela inicial">
    <img src="./.prints/gerar-senha.jpeg" width="150" title="Senha aleatoria Criada">
    <img src="./.prints/listar-senhas.jpeg" width="150" title="Lista de senhas">
    <img src="./.prints/senha-oculta.jpeg" width="150" title="Ocultar senha">
</center>