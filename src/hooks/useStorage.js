import AsyncStorage from "@react-native-async-storage/async-storage";

const useStorage = () => {
    // BUSCAR OS ITENS SALVOS 

    const getItens = async (key) => {

        try {
            const textostorage = await AsyncStorage.getItem(key);
            let converteTextostorageEmJson = JSON.parse(textostorage);

            if (converteTextostorageEmJson != "" && converteTextostorageEmJson != null) {
                return converteTextostorageEmJson;
            }else{
                return [];
            }

        } catch (error) {
            console.log("Erro ao buscar", error)
            return[];
        }
    }

     // SALVAR UM ITEM NO STORAGE 

    const saveItem = async ( key, value ) => {

        try {

            let  passwords = await getItens(key);
        
            passwords.push(value)

            await AsyncStorage.setItem(key, JSON.stringify(passwords))
            

        } catch (error) {
            console.log("Erro ao salvar", error)
            
        }
        
    }

    // REMOVER ALGO DO STORAGE

    const removeItem = async (key, item) => {

        try {
            let  passwords = await getItens(key);

            let myPassword = passwords.filter( ( password ) => {
                return (password !== item)
            })
            
            await AsyncStorage.setItem(key, JSON.stringify(myPassword))

            return myPassword;
            
        } catch (error) {
            console.log("Erro ao deletar", error)
        }
    }

    return {
        getItens,
        saveItem,
        removeItem,
    };



}


export default useStorage;