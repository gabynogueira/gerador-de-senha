import { useState,useEffect } from 'react'
import { Alert, FlatList, StyleSheet, Text, View } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { useIsFocused } from '@react-navigation/native';
import  useStorage  from '../../hooks/useStorage'
import { PasswordItem } from '../PasswordItem';



export function Password(){
    const [ listPasswords, setListPasswords] = useState([])
    const focused = useIsFocused();
    const { getItens, removeItem } = useStorage();

    async function handleDeletePassword(item){
      
        

        Alert.alert(
            'Senha',
            'Deseja excluir essa senha?'+ item,  
            [
               {text: 'Cancelar', onPress: () => console.log('Cancelar'), style: 'cancel'},
               {text: 'Excluir', onPress: async () => {
                    const listaAtualizada = await removeItem("@pass",item);

                    setListPasswords(listaAtualizada)

                    Alert.alert("Senha excluida")
               }},
            ],
            { cancelable: false }
       )





    }

    useEffect(() => {

        async function loadPasswords(){
            
            const passwords = await getItens("@pass")
            // console.log(passwords);
            setListPasswords(passwords);
        }

        loadPasswords();

    }, [focused])
    
    return(
        <SafeAreaView style={{ flex: 1, }}>
            <View style={styles.header}>
                <Text style={styles.title}> Minhas Senhas </Text>
            </View>


            <View style={styles.content}>
                <FlatList style= {{ flex:1, paddingTop: 14 }}
                    data={listPasswords}
                    renderItem={({ item }) => <PasswordItem data= {item} removePassword={ () => {handleDeletePassword(item)}}/>}
                    keyExtractor={ (item) => String(item)}
                />

            </View>

            {
                // listPasswords.map((value, index) => (
                //     <Text>
                //         {value}
                //     </Text>
                // ))
                
            
            }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    header: { 
        backgroundColor: "#392de9",
        paddingTop: 58,
        paddingBottom: 14,
        paddingHorizontal: 14
    },
    title: {
        fontSize: 25,
        color: "#fff",
        fontWeight: "bold",

    },
    content: {
        flex: 1,
        paddingHorizontal: 14,

    }
    

})