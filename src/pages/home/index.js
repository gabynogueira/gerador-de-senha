import { useState } from "react";
import Slider from "@react-native-community/slider";
import { StyleSheet, Text, View, Image, TouchableOpacity, Modal } from "react-native";
import { ModalPassword } from "../../components/modal";


let possibilidades = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"



export function Home(){
  const [limiteCaracteres, setLimiteCaracteres] = useState(10)
  const [passwordValue, setPasswordValue] = useState("")
  const [modalVisible, setModalVisible] = useState(false);


  function generatePassword(){
    
    let senha = "a";
    let totalDePossibilidades = possibilidades.length;

    for (let inicio = 0; inicio < limiteCaracteres; inicio++ ) {

      let formula = Math.random()*totalDePossibilidades;
      let formulaArredondada = Math.floor(formula);

      novoCaracter = possibilidades.charAt(formulaArredondada);
      senha = senha+novoCaracter;

    }




    setPasswordValue(senha);
    setModalVisible(true);
    
    // for(let i = 0, n = charset.length; i < size; i++) { 
      // password += charset.charAt(Math.floor(Math.random() * n))
    //  }
    //  console.log(password)
  }


  return(
    <View style={styles.container}>
      <Image
        source={require("../../assets/logo.png")}
        style={styles.logo}
      />

      <Text style={styles.title}>{limiteCaracteres} Caracteres</Text>
      <View style={styles.area}>
        <Slider
           style={{ height: 50}}
           minimumValue={6}
           maximumValue={20}
           maximumTrackTintColor="#ff0000"
           minimumTrackTintColor="#000"
           thumbTintColor="#392de9"
           value={limiteCaracteres}
           onValueChange={ (value) => setLimiteCaracteres(value.toFixed(0)) }
           

        />
      </View>
      
      <TouchableOpacity style={ styles.button } onPress={generatePassword}>
        <Text style={ styles.buttonText }>
          Gerar Senha 
        </Text>

      </TouchableOpacity>

      <Modal visible={modalVisible} animationType="fade" transparent={true} statusBarTranslucent >
        <ModalPassword password={passwordValue} handleClose={ () => setModalVisible( false )} />
      </Modal>

    </View>
  )
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor: "#f3f3ff",
    justifyContent: "center",
    alignItems: "center"
  },
  logo: {
    marginBottom: 60,
  },
  title: {
    fontSize: 30,
    fontWeight: "bold",

  },
  area: {
    marginTop: 14,
    marginBottom: 14,
    width:"80%",
    backgroundColor:"#fff",
    borderRadius: 8,
    padding: 6, 


  },
  button: {
    backgroundColor: "#392de9",
    width:"80%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
    marginBottom: 18,
  },
  buttonText:{
    color: "#fff",
    fontSize: 20,

  },

})