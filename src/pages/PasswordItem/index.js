
import { Pressable, StyleSheet, Text, TouchableOpacity, View, } from "react-native";
import { Ionicons} from '@expo/vector-icons'
import { useState } from "react";

export function PasswordItem({ data, removePassword }){

  var [ podeVe, setPodeVe ] = useState(true) 

  function verSenha(){
    
    // if(podeVe == true){
    //   setPodeVe(false)
    // }else {
    //   setPodeVe(true)
    // }
    setPodeVe(!podeVe)

  }

  return(
    <View style={styles.container}>
        <Pressable
          style={{ height: 25, width: '90%' }} 
          onLongPress={removePassword}
        >
          <Text style={styles.text}>
            
            { 
              podeVe == true ? data : "*******"
            }
          
          </Text>

        </Pressable>

        <TouchableOpacity 
          style={{ padding: 8}}
          onPress={()=> {setPodeVe(!podeVe)}}
        >
          {
            podeVe == true ? <Ionicons size={20} color={"#fff"} name="eye" /> : <Ionicons size={20} color={"#fff"} name="eye-off-sharp"/>
          }
          
        </TouchableOpacity>
        
    </View>
   
  )
}

const styles = StyleSheet.create({
  container:{
    backgroundColor: "#0e0e0e",
    padding: 14,
    width: "100%",
    marginBottom: 14,
    borderRadius: 8,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",

    
  },
  text: {
    color: "#fff",
  }

})